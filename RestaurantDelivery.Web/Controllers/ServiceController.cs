﻿using RestaurantDelivery.Core.Contracts;
using RestaurantDelivery.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Practices.Unity;
using System.Linq;
using System.Net.Http;
using System.Net;

namespace RestaurantDelivery.Web.Controllers
{
    public class ServiceController : ApiController
    {
        private IRestaurantService _service;
        public ServiceController(IRestaurantService rs)
        {
            _service = rs;
        }
       
        [HttpGet()]
        public  List<Restaurant> GetRestaurants([FromUri] string Postcode)
        {
            RestaurantRoot res = _service.GetRestaurants(Postcode);
            if (!res.HasErrors)
            {
                var filteredList = (from f in res.Restaurants
                                    orderby f.RatingAverage descending
                                    select f
                              ).ToList();
              
                return filteredList;
            }
            else
            {
                //Create a bad request http response
                HttpResponseMessage errResponse = Request.CreateResponse(HttpStatusCode.BadRequest, new Error()
                {
                    Message = "Could not decode request: JSON parsing failed"
                });
                throw new HttpResponseException(errResponse);
            }
        }
    }
}
