﻿using RestaurantDelivery.Core.Contracts;
using RestaurantDelivery.Model;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Practices.Unity;

namespace RestaurantDelivery.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index([FromUri] string Postcode)
        {
            ViewBag.Title = "Home Page";
            var _service = IntegrationManager.Instance.RegisterContainer().Resolve<IRestaurantService>();
            RestaurantRoot res = _service.GetRestaurants(Postcode);
            if (!res.HasErrors)
            {
                var filteredList = (from f in res.Restaurants
                                    orderby f.RatingAverage descending
                                    select f
                              ).ToList();
                ViewBag.TotalCount = filteredList.Count;
                ViewBag.Area = res.Area;
                return View(filteredList);
            }
            else
            {
                ViewBag.Error = (from i in res.Errors select i.Message).FirstOrDefault();
                return View();
            }

        }

    }
}
