﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;

namespace RestaurantDelivery.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            MediaTypeFormatterCollection mediaTypeFormatters = config.Formatters;
            mediaTypeFormatters.Remove(mediaTypeFormatters.XmlFormatter);

            // Web API routes

            config.Routes.MapHttpRoute(
                name: "api",
                routeTemplate: "api/{controller}/{action}/"
              );

        }
    }
}
