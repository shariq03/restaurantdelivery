﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace RestaurantDelivery.Model
{
    public class Restaurant
    {

        public double RatingAverage { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
        public string Postcode { get; set; }
        public List<Logo> Logo { get; set; }
        public List<Menu> Menus { get; set; }
    }
    public class Logo
    {
        public string StandardResolutionURL { get; set; }
    }
    public class RestaurantRoot
    {
        public List<Restaurant> Restaurants { get; set; }
        public string ShortResultText { get; set; }
        public string Area { get; set; }
        public List<Error> Errors { get; set; }
        public bool HasErrors { get; set; }


    }
}