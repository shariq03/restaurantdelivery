﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Model
{
    public class ProductRoot
    {
        public List<Product> Products;
    }
    public class Product
    {
        public int  Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
