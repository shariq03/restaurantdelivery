﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Model
{
    public class Header
    {
        public string AcceptTenant { get; set; }
        public string AcceptLanguage { get; set; }
        public string AcceptCharset { get; set; }
        public string Signature { get; set; }
        public string Host { get; set; }
    }
}
