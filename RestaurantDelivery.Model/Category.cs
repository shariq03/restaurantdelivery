﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Model
{
    public class CategoryRoot
    {
        public List<Category> Categories;
    }
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
