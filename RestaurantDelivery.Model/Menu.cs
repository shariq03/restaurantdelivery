﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Model
{
    public class Menu
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<Product> Products { get; set; }
    }

    public class MenuRoot
    {
        public List<Menu> Menus { get; set; }
    }

}
