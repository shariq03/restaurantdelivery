﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Model
{
    public class Error
    {
        public string ErrorType { get; set; }
        public string Message { get; set; }
    }
}
