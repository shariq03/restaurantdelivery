﻿using RestaurantDelivery.Core.Contracts;
using Microsoft.Practices.Unity;
using RestaurantDelivery.Communication;

namespace RestaurantDelivery.Core.Implementations
{
    public class UnityRegistryHelper : IUnityRegistryHelper
    {
        private static UnityContainer container = new UnityContainer();
        public UnityContainer RegisterContainer()
        {
            if (!container.IsRegistered(typeof(IAuthorisationHeader)))
            {
                container.RegisterType<IAuthorisationHeader, AuthorisationHeader>();
            }
            if (!container.IsRegistered(typeof(IRestaurantService)))
            {
                container.RegisterType<IRestaurantService, RestaurantService>();
            }
            if (!container.IsRegistered(typeof(IServiceHandler)))
            {
                container.RegisterType<IServiceHandler, ServiceHandler>();
            }
            return container;
        }

    }
}
