﻿using RestaurantDelivery.Core.Contracts;
using RestaurantDelivery.Model;
using RestaurantDelivery.Communication;
using System.Collections.Generic;

namespace RestaurantDelivery.Core.Implementations
{
    public class RestaurantService : IRestaurantService
    {
        private IServiceHandler _service;
        /// <summary>
        /// Initializes a new instance of the <see cref="RestaurantService"/> class.
        /// </summary>
        /// <param name="_serviceHandle">The _service handle.</param>
        public RestaurantService(IServiceHandler _serviceHandle)
        {
            _service = _serviceHandle;
        }
        /// <summary>
        /// Gets the restaurants.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public RestaurantRoot GetRestaurants(string postCode)
        {
            RestaurantRoot response = null;
            if (!string.IsNullOrEmpty(postCode))
            {
                response = _service.Call<RestaurantRoot>(string.Format("restaurants?q={0}", postCode)).Result;
            }
            else
            {
                List<Error> err = new List<Error>();
                err.Add(new Error() { Message = "Postcode can not be null or empty." });
                response = new RestaurantRoot() { HasErrors = true, Errors = err };
            }

            return response;
        }

        /// <summary>
        /// Gets the menus.
        /// </summary>
        /// <param name="restaurantId">The restaurant identifier.</param>
        /// <returns></returns>
        public MenuRoot GetMenus(int restaurantId)
        {
            //Resolving dependency
            MenuRoot response = null;
            if (restaurantId > 0)
            {
                response = _service.Call<MenuRoot>(string.Format("restaurants/{0}/menus", restaurantId)).Result;
            }
            return response;
        }
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="menuId">The menu identifier.</param>
        /// <returns></returns>
        public CategoryRoot GetCategories(int menuId)
        {
            //Resolving dependency

            var response = _service.Call<CategoryRoot>(string.Format("menus/{0}/productcategories ", menuId));
            return response.Result;
        }
        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="menuId">The menu identifier.</param>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        public ProductRoot GetProducts(int menuId, int categoryId)
        {
            //Resolving dependency

            var response = _service.Call<ProductRoot>(string.Format("menus/{0}/productcategories/{1}/products", menuId, categoryId));
            return response.Result;
        }
    }
}