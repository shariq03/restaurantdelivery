﻿using RestaurantDelivery.Core.Contracts;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using RestaurantDelivery.Library;
using System.Net.Http.Headers;

namespace RestaurantDelivery.Communication
{
    public class ServiceHandler : IServiceHandler
    {
        private IAuthorisationHeader _authorisation;
        public ServiceHandler(IAuthorisationHeader auth)
        {
            _authorisation = auth;
        }
        public async Task<T> Call<T>(string resource)
        {
            //resolve dependency injection for IAuthorisationHeader
            var header = _authorisation.GetRequestHeader();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new System.Uri(ApiEndPoint.ApiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                //generate header 
                httpClient.DefaultRequestHeaders.Add("Accept-Tenant", header.AcceptTenant);
                httpClient.DefaultRequestHeaders.Add("Accept-Language", header.AcceptLanguage);
                httpClient.DefaultRequestHeaders.Add("Accept-Charset", header.AcceptCharset);
                httpClient.DefaultRequestHeaders.Add("Authorization", header.Signature);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //call web service asynchronously 
                using (var response = Task.Run(
                    async () => await httpClient.GetAsync(resource))
                    .GetAwaiter()
                    .GetResult())
                {
                    //converting the response into generic object supplied from origional request
                    using (var content = response.Content)
                    {
                        var reponse = await content.ReadAsAsync<T>();
                        return reponse;
                    }
                }
            }
        }
    }
}
