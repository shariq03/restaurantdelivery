﻿using RestaurantDelivery.Core.Contracts;
using RestaurantDelivery.Model;
using System.Collections.Generic;

namespace RestaurantDelivery.Core.Implementations
{
    public class AuthorisationHeader : IAuthorisationHeader
    {
        public Header GetRequestHeader()
        {
            //TODO: get this from config or database repository
            Header header = new Header();
            header.AcceptTenant = "uk";
            header.AcceptLanguage = "en-GB";
            header.AcceptCharset = "utf8";
            header.Signature = "Basic VGVjaFRlc3RBUEk6dXNlcjI=";
            header.Host = "public.je-apis.com";
            return header;
        }

    }
}