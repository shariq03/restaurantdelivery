﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Communication
{
    public interface IServiceHandler
    {
        Task<T> Call<T>(string resource);
    }
}
