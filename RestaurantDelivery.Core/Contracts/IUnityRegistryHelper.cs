﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices;
using Microsoft.Practices.Unity;

namespace RestaurantDelivery.Core.Contracts
{
    public interface IUnityRegistryHelper
    {
        UnityContainer RegisterContainer();
        
    }
}
