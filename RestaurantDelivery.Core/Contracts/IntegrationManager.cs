﻿using RestaurantDelivery.Core.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Core.Contracts
{
    public class IntegrationManager
    {
        private static IUnityRegistryHelper _register;
        //make construction private so it can not be intantiated
        private IntegrationManager()
        {

        }

        public static IUnityRegistryHelper Instance
        {
            get
            {
                if (_register == null)
                {
                    _register = new UnityRegistryHelper();
                }
                return _register;
            }
        }

    }
}
