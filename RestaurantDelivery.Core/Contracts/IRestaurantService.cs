﻿using RestaurantDelivery.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantDelivery.Core.Contracts
{
    public interface IRestaurantService
    {
        RestaurantRoot GetRestaurants(string postCode);
        MenuRoot GetMenus(int restaurantId);
        CategoryRoot GetCategories(int menuId);
        ProductRoot GetProducts(int menuId,int categoryId);
    }
}