﻿using RestaurantDelivery.Model;
using System.Collections.Generic;

namespace RestaurantDelivery.Core.Contracts
{
    public interface IAuthorisationHeader
    {
        Header GetRequestHeader();
    }
}