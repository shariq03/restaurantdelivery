﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantDelivery.Library
{
    public static class ApiEndPoint
    {
        //making this api end point as ready only so if it is required to change during runtime from configuration file we could change
        //https://public.je-apis.com
        public static readonly string ApiBaseUrl = "https://public.je-apis.com/";
       
    }
}
