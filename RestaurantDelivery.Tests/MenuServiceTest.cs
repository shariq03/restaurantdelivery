﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantDelivery.Model;
using System.Collections.Generic;
using RestaurantDelivery.Core.Implementations;
using RestaurantDelivery.Communication;

namespace RestaurantDelivery.Tests
{
    [TestClass]
    public class MenuServiceTest
    {
        private MenuRoot _MenuRepository;

        [TestMethod]
        public void Menu_Return_ReturnOneOrMoreMenus()
        {
            int restaurantId = 19413;
            var serviceMock = new Mock<IServiceHandler>();
            SetupMenus();
            var moqOb = serviceMock.Setup(s =>
            s.Call<MenuRoot>(string.Format("restaurants/{0}/menus", restaurantId))).ReturnsAsync(_MenuRepository);

            var service = new RestaurantService(serviceMock.Object);
            var getMenu = service.GetMenus(restaurantId);
            Assert.AreEqual(true, getMenu.Menus.Count > 0);
        }

        public void SetupMenus()
        {
            List<Menu> list = new List<Menu>();
            _MenuRepository = new MenuRoot();
            list.Add(new Menu() { Title = "Collection night", RestaurantId = 19413, Id = 63483 });
            _MenuRepository.Menus = list;
        }
    }
}
