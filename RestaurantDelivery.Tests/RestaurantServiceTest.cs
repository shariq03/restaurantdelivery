﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantDelivery.Model;
using System.Collections.Generic;
using RestaurantDelivery.Core.Implementations;
using RestaurantDelivery.Communication;
using System.Linq;

namespace RestaurantDelivery.Tests
{
    [TestClass]
    public class RestaurantServiceTest
    {
        private RestaurantRoot _RestaurantRepository;

        [TestMethod]
        public void Restaurants_Return_PostCodeShouldNotBeNullOrEmpty()
        {

            var serviceMock = new Mock<IServiceHandler>();

            var moqOb = serviceMock.Setup(s =>
             s.Call<RestaurantRoot>(string.Format("restaurants?q={0}", ""))).Returns(() => null);

            var service = new RestaurantService(serviceMock.Object);
            var getRest = service.GetRestaurants("");
            Assert.AreEqual("Postcode can not be null or empty.", getRest.Errors.FirstOrDefault().Message);
        }

        [TestMethod]
        public void Restaurants_Return_ReturnOneOrMoreRestaurants()
        {
            string postCode = "SE19";
            var serviceMock = new Mock<IServiceHandler>();
            SetupRestaurants();
            var moqOb = serviceMock.Setup(s =>
            s.Call<RestaurantRoot>(string.Format("restaurants?q={0}", postCode))).ReturnsAsync(_RestaurantRepository);

            var service = new RestaurantService(serviceMock.Object);
            var getRest = service.GetRestaurants(postCode);
            Assert.AreEqual(true, getRest.Restaurants.Count > 0);
        }

        public void SetupRestaurants()
        {
            List<Restaurant> list = new List<Restaurant>();
            _RestaurantRepository = new RestaurantRoot();
            list.Add(new Restaurant() { Name = "Pizza Rossa", RatingAverage = 4.02, Id = 12344, Url = "http//:google.com" });
            list.Add(new Restaurant() { Name = "Pizza Italiano", RatingAverage = 3.02, Id = 124, Url = "http//:yahoo.com" });
            _RestaurantRepository.Restaurants = list;
        }
    }
}
