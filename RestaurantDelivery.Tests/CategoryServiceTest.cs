﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantDelivery.Model;
using System.Collections.Generic;
using RestaurantDelivery.Core.Implementations;
using RestaurantDelivery.Communication;

namespace RestaurantDelivery.Tests
{
    [TestClass]
    public class CategoryServiceTest
    {
        private CategoryRoot _CategoryRepository;

        [TestMethod]
        public void Category_Return_ReturnOneOrMoreCategories()
        {
            int menuId = 1;
            var serviceMock = new Mock<IServiceHandler>();
            SetupCategories();
            var moqOb = serviceMock.Setup(s =>
            s.Call<CategoryRoot>(string.Format("menus/{0}/productcategories ", menuId)))
            .ReturnsAsync(_CategoryRepository);

            var service = new RestaurantService(serviceMock.Object);
            var getCategories = service.GetCategories(menuId);
            Assert.AreEqual(true, getCategories.Categories.Count > 0);
        }

        public void SetupCategories()
        {
            List<Category> list = new List<Category>();
            _CategoryRepository = new CategoryRoot();
            list.Add(new Category() { Name = "Pizza" });
            _CategoryRepository.Categories = list;
        }
    }
}
