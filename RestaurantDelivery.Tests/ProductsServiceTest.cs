﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantDelivery.Model;
using System.Collections.Generic;
using RestaurantDelivery.Core.Implementations;
using RestaurantDelivery.Communication;

namespace RestaurantDelivery.Tests
{
    [TestClass]
    public class ProductsServiceTest
    {
        private ProductRoot _ProductRepository;

        [TestMethod]
        public void Product_Return_ReturnOneOrMoreProducts()
        {
            int menuId = 1;
            int categoryId = 1;
            var serviceMock = new Mock<IServiceHandler>();
            SetupProducts();
            var moqOb = serviceMock.Setup(s =>
            s.Call<ProductRoot>(string.Format("menus/{0}/productcategories/{1}/products", menuId, categoryId))).ReturnsAsync(_ProductRepository);

            var service = new RestaurantService(serviceMock.Object);
            var getRest = service.GetProducts(menuId, categoryId);
            Assert.AreEqual(true, getRest.Products.Count > 0);
        }

        public void SetupProducts()
        {
            List<Product> list = new List<Product>();
            _ProductRepository = new ProductRoot();
            list.Add(new Product() { Name = "Pizza Rossa" });
            _ProductRepository.Products = list;
        }
    }
}
